#  A simple web crawler built with Go

## Instructions

We'd like you to write a simple web crawler in a programming language of your choice. Feel free to either choose one you're very familiar with or, if you'd like to learn some Go, you can also make this your first Go program! The crawler should be limited to one domain - so when you start with https://monzo.com/, it would crawl all pages within monzo.com, but not follow external links, for example to the Facebook and Twitter accounts. Given a URL, it should print a simple site map, showing the links between pages.

Ideally, write it as you would a production piece of code. Bonus points for tests and making it as fast as possible!

## Compile

` go build `

## Test

` go test `

## Run

`./sitemap` to display the command line usage .

`./sitemap https://monzo.com` to generate a sitemap for Monzo website

## Crawler results

All sitemaps are generated inside the sitemaps folder by default. You can change the path by setting --filepath

## Example

To crawl a website (https://monzo.com) and print a simple sitemap run:

` ./sitemap https://monzo.com `

## Subdomains

The crawler does not follow subdomain links found in the main website.

All URLs listed in the Sitemap must reside on the same host as the Sitemap. For instance, if the Sitemap is located at http://www.example.com/sitemap.xml, it can't include URLs from http://subdomain.example.com. If the Sitemap is located at http://www.example.com/myfolder/sitemap.xml, it can't include URLs from http://www.example.com.

[sitemaps.org](https://www.sitemaps.org/faq.html#faq_sitemap_location)

You can crawl a subdomain though and generate a sitemap by running.

` ./sitemap https://web.monzo.com `
