package crawler

import (
	"net/url"
	"sitemap/options"
	"testing"
)

func TestRun(t *testing.T) {
	type args struct {
		o options.CrawlOpts
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "monzo",
			args: args{
				options.CrawlOpts{
					URL: &url.URL{
						Scheme: "https", Host: "monzo.com"},
					Depth:    2,
					Filename: "../sitemaps/monzo.com.txt",
					Storage:  options.Enum.File,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Run(tt.args.o)
		})
	}
}

func TestSitemap(t *testing.T) {
	type args struct {
		o options.CrawlOpts
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "monzo",
			args: args{
				options.CrawlOpts{
					URL: &url.URL{
						Scheme: "https", Host: "monzo.com"},
					Depth:    2,
					Filename: "../sitemaps/monzo.com.txt",
					Storage:  options.Enum.File,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Sitemap(tt.args.o)
		})
	}
}
