package file

import (
	"bufio"
	"fmt"
	"os"
	"sync"
)

//Sitemap save links to file
func Sitemap(links *sync.Map, filepath string) {
	file, err := os.Create(filepath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	w := bufio.NewWriter(file)

	links.Range(func(k, v interface{}) bool {
		fmt.Fprintln(w, v)
		return true
	})

	w.Flush()
}
